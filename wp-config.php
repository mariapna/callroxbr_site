<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */
// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'projetos_callroxbr_site');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');
/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'd&5Qg8r`le;4hQTDfotT0&.ybO!%NKDtKy(~O@9)GCT{A0Ecqw/E~XW)8y,`b al');
define('SECURE_AUTH_KEY',  'LA[,1e;lnOe(dx<.aj4NU1MD8=uaIvkfYp2J_P-mWY.q?K:T;9W$t&RLXlCWbVmi');
define('LOGGED_IN_KEY',    '/.CqGf:(z#KX{(_,9=74N1ql@P11^A[>H_mh}Xc:9<7h*0)?Q3Bwa1~#PshGBpR~');
define('NONCE_KEY',        'U)~ys( .Eeb;^k%cfxm]][nczZr6s!oz ILQteIrwQAjb^#LD0pk_f2hX+4+}Y3v');
define('AUTH_SALT',        '.sPU1Gg?Fi(;_gNp%TK+b9-Aun ZpmMWy_bT}DCxQHO.pAKC IZ?|9xL#{jNRp)<');
define('SECURE_AUTH_SALT', '4LCZqZ[,OC2J(N]gKHKCuJ6eQr@=8-X3_]z+Pvr)t6?iW{thx*s2=6TYHhrt-,|R');
define('LOGGED_IN_SALT',   'CzW!dJ)2F,7aGB/fl;E>+Zi&LY/DM0&JQe~~W%N+Wlt94tsWaM$-NZ#%O[b#(9K0');
define('NONCE_SALT',       '2.c@drgAF@}5d7{h-X1n`A?Yxfh$a>KzmWx>nO7Ir{+mxsap,eUP;`kL+c;Im-3l');
/**#@-*/
/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'cx_';
/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);
/* Isto é tudo, pode parar de editar! :) */
/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
