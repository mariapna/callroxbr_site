<?php
/**
 * Template name: Inicial
 * Template part for initial page
 *
 * 
 *
 * @package salesrox
 */
get_header();
?>

<!-- PG INICIAL -->
<div class="pg pg-inicial">
	<section class="inicial">
		<div class="containerFull">
			<h6 class="hidden">Sessão Inicial</h6>
			<div class="textoInicial">
				<h2><?php echo $configuracao['opt_titulo']; ?></h2>
				<p><?php echo $configuracao['opt_descricao']; ?></p>
			</div>
			<div class="formularioContato">
				<?php echo do_shortcode('[contact-form-7 id="15" title="Formulário de contato"]'); ?>
			</div>
		</div>
	</section>
	<div class="scrollDown">
		<a href="#comoFunciona" class="scrollTop"></a>
	</div>

	<section class="comoFunciona" id="comoFunciona">
		<div class="containerFull overX">
			<div class="row">
				<div class="col-md-6">
					<div class="textoComoFunciona">
						<h3><?php echo $configuracao['opt_comoFunciona_titulo']; ?></h3>
						<?php echo $configuracao['opt_comoFunciona_conteudo']; ?>
					</div>
					<div class="tecnologias fora">
						<ul>
							<li>
								<img src="<?php echo get_template_directory_uri(); ?>/img/trsaudio.png" alt="Transcrição de Áudio">
								<p>Audio Transcription</p>
							</li>
							<li>
								<img src="<?php echo get_template_directory_uri(); ?>/img/ia.png" alt="Transcrição de Áudio">
								<p>Artificial Intelligence</p>
							</li>
							<li>
								<img src="<?php echo get_template_directory_uri(); ?>/img/coaching.png" alt="Transcrição de Áudio">
								<p id="coaching">Coaching Features</p>
							</li>
						</ul>
					</div>
					<div class="textoComoFunciona">
						<?php echo $configuracao['opt_comoFunciona_conteudo2']; ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="bordaVantagens"></div>
					<div class="vantagesSalesrox">
						<h2><?php echo $configuracao['opt_vantagens_titulo']; ?></h2>
						<div class="vantagens fora">
							<ul>
								<li>
									<article>
										<figure class="imagemDestaque">
											<img src="<?php echo $configuracao['opt_vantagens_topico_1_img']['url']; ?>" alt="Conversão">
										</figure>
										<h1><?php echo $configuracao['opt_vantagens_topico_1_titulo']; ?></h1>
									</article>
								</li>
								<li>
									<article>
										<figure class="imagemDestaque">
											<img src="<?php echo $configuracao['opt_vantagens_topico_2_img']['url']; ?>" alt="Conversão">
										</figure>
										<h1><?php echo $configuracao['opt_vantagens_topico_2_titulo']; ?></h1>
									</article>
								</li>
								<li>
									<article>
										<figure class="imagemDestaque">
											<img src="<?php echo $configuracao['opt_vantagens_topico_3_img']['url']; ?>" alt="Conversão">
										</figure>
										<h1><?php echo $configuracao['opt_vantagens_topico_3_titulo']; ?></h1>
									</article>
								</li>
								<li>
									<article>
										<figure class="imagemDestaque">
											<img src="<?php echo $configuracao['opt_vantagens_topico_4_img']['url']; ?>" alt="Conversão">
										</figure>
										<h1><?php echo $configuracao['opt_vantagens_topico_4_titulo']; ?></h1>
									</article>
								</li>
							</ul>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="algumasLogos">
		<div class="containerFull">
			<ul class="listadeLogos">
				<li><a href="<?php echo $configuracao['opt_logos1_link'] ?>"><img src="<?php echo $configuracao['opt_logos1']['url'] ?>" alt=""></a></li>
				<li><a href="<?php echo $configuracao['opt_logos2_link'] ?>"><img src="<?php echo $configuracao['opt_logos2']['url'] ?>" alt=""></a></li>
				<li><a href="<?php echo $configuracao['opt_logos3_link'] ?>"><img src="<?php echo $configuracao['opt_logos3']['url'] ?>" alt=""></a></li>
				<li><a href="<?php echo $configuracao['opt_logos4_link'] ?>"><img src="<?php echo $configuracao['opt_logos4']['url'] ?>" alt=""></a></li>
				<li><a href="<?php echo $configuracao['opt_logos5_link'] ?>"><img src="<?php echo $configuracao['opt_logos5']['url'] ?>" alt=""></a></li>
			</ul>
		</div>
	</div>
</div>

<?php get_footer(); ?>