<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package salesrox
 */
global $configuracao;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta name="google-site-verification" content="MbyKDv4pmteOBFRr4XhXpdmi-_c_ygprQTWbY2IpLWg" />
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121354164-13"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-121354164-13');
	</script>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- META -->
	<!-- <meta name="description" content="">
	<meta name="keywords" content="">
	<meta property="og:title" content="Salesrox" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="" />
	<meta property="og:image" content=""/>
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="" /> -->

	<!-- TÍTULO -->
	<!-- <title>CallRox  </title> -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129593932-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-129593932-1');
	</script>

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" /> 
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- TOPO -->
	<header class="topo">
		<div class="row">
			<!-- LOGO -->
			<div class="col-xs-6">
				<a href="<?php echo get_home_url() ?>">
					<img class="img-responsive" src="<?php echo $configuracao['opt_logo']['url']; ?>" alt="Salesrox">
				</a>
			</div>
			<!-- MENU  -->	
			<div class="col-xs-6">
				<div class="language">
					<ul>
						<li class="selected">
							<span>BR</span>
							<ul>
								<li>
									<a href="https://callrox.com">
										<span>EN</span>
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</header>
