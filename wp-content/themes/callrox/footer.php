<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package salesrox
 */
global $configuracao;
?>

<!-- RODAPÉ -->
<footer class="rodape">
	<div class="containerFull">
		<div class="tituloFormulario">
			<h1><strong><?php echo $configuracao['opt_rodape_titulo']; ?></strong></h1>
			<p><?php echo $configuracao['opt_rodape_subtitulo']; ?></p>
		</div>
	</div>
	<div class="formularioContato">
		<div class="containerFull">
			<?php echo do_shortcode('[contact-form-7 id="15" title="Formulário de contato"]'); ?>
        </div>
	</div>
	<div class="copyright">
		<p><?php echo $configuracao['opt_rodape_copyright']; ?></p>
	</div>
</footer>

<?php wp_footer(); ?>


</body>
</html>
